# Frontend Mentor - 3 Column Card solution

This is a solution to the [3 Column Card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/3column-preview-card-component-pH92eAR2-). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

This is a basic project, but it was done to:

- Practice fundamental concepts, css and html
- Work with Vite
- Deploy to Gitlab pages

### Links

- Live Site URL: [Live Solution](https://frontendchallenges.gitlab.io/3-column-card/)

## My process

1. This is a simple html, js and sass project. Uses node to compile sass to css
2. Create tokens.scss with the constant values for colors, fonts, and sizes, and styles.scss for general styling
   The styles.scss must be included in main.js and tokens.scss
3. Think of the design by splitting the layout in Components:

```
    Columns:
      Card (sedan): img, title, quote, button
      Card (suvs)
      Card (luxury)
```

6. Change columsn disposition on desktop view
7. Deploy to gitlab pages (remember configure the .gitlab-ci.yml)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Vue + Vite

### Lessons

How to change columns(layout) disposition

```css
.columns {
  display: flex;
  flex-direction: column;
}
@media screen and (min-width: $md-device) {
  .columns {
    flex-direction: row;
  }
}
```

### Continued development

This is one of many projects, I plan to build to make more experience with FrontEnd development, specially with the Framework, handling spacing and practicing css.

### Useful resources

- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.
